package gov.nasa.marsexp.test;

import gov.nasa.marsexp.model.IRobot;
import gov.nasa.marsexp.model.ISurface;
import gov.nasa.marsexp.model.geo.Coordinate;
import gov.nasa.marsexp.model.geo.Position;
import gov.nasa.marsexp.model.impl.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public final class TestRobot {

    Coordinate coordinate;
    Position position;
    ISurface surface;
    IRobot robot;

    @Before
    public void setUp() throws Exception {
        coordinate = new Coordinate(0, 0);
        position = new Position(Position.Direction.NORTH);
        surface = Surfaces.newMarsInstance(5, 5);
        robot = Robots.newInstance(surface, coordinate, position);
        surface.addRobot(robot);
    }

    @Test
    public void testRobotIsTurningToLeftAndTurningToRight() {
        // test that the robot started in the right position
        Assert.assertNotNull(robot.getPosition());
        Assert.assertNotNull(robot.getCoordinate());
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // rotate 360 degree rotation turning to left one step at each time
        robot.execute("L");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.WEAST);
        robot.execute("L");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.SOUTH);
        robot.execute("L");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.EAST);
        robot.execute("L");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // rotate 360 degree rotation turning to left with one command
        robot.execute("LLLL");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // rotate 360 degree rotation turning to right one step at each time
        robot.execute("R");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.EAST);
        robot.execute("R");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.SOUTH);
        robot.execute("R");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.WEAST);
        robot.execute("R");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // rotate 360 degree rotation turning to right with one command
        robot.execute("RRRR");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // turn to left and turn to right in the same command (back to the same position)
        robot.execute("LR");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);

        // turn to right and turn to left in the same command (back to the same position)
        robot.execute("RL");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
    }

    @Test
    public void testRobotIsMoving() {

        // rotate right and move 4 positions on X axis
        robot.execute("RMMMM");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.EAST);
        Assert.assertEquals(robot.getCoordinate().getX(), 4);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);

        // back to the origin
        robot.execute("LLMMMMR");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);

        // move 4 positions on Y axis
        robot.execute("MMMM");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 4);

        // back to the origin
        robot.execute("RRMMMMLL");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);

        // move with rotations to right
        robot.execute("MMRMMRMM");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.SOUTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 2);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);

        // back to the origin
        robot.execute("RMMR");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);

        // move and turn left
        robot.execute("MML");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.WEAST);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 2);

        // back to the origin
        robot.execute("LMMRR");
        Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
        Assert.assertEquals(robot.getCoordinate().getX(), 0);
        Assert.assertEquals(robot.getCoordinate().getY(), 0);
    }

    @Test
    public void testRobotIsMovingOnlyWithValidCommands() {
        // invalid commands - don't move!!
        try {
            robot.execute("RMAMMM");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            robot.execute("");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            robot.execute(null);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            robot.execute("AAAAA");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            robot.execute("BMMML");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            robot.execute("MMMLB");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }
    }

    @Test
    public void testRobotIsMovingOnlyWhenTheCommandCanLetTheRobotInAValidPosition() {
        // invalid commands - don't move!!
        try {
            // move to y = 5
            robot.execute("MMMMM");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            // move to x = 5
            robot.execute("RMMMMM");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            // move to x = 5 in baby steps - should be in the extreme x of the surface
            robot.execute("R");
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.EAST);
            Assert.assertEquals(robot.getCoordinate().getX(), 4);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
            // go back home
            robot.execute("LLMMMMR");
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            // move to y = 5 in baby steps - should be in the extreme y of the surface
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
            robot.execute("M");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 4);
            // go back home
            robot.execute("RRMMMMLL");
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            // move to a negative point in x
            robot.execute("LM");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }

        try {
            // move to a negative point in x
            robot.execute("RRM");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(robot.getPosition().getDirection(), Position.Direction.NORTH);
            Assert.assertEquals(robot.getCoordinate().getX(), 0);
            Assert.assertEquals(robot.getCoordinate().getY(), 0);
        }
    }
}
