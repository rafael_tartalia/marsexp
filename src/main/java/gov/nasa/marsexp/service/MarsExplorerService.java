package gov.nasa.marsexp.service;


import gov.nasa.marsexp.model.ISurface;

import javax.inject.Inject;

/**
 * Mars services exposed.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public final class MarsExplorerService {

    @Inject
    private ISurface mars;

    public void execute(String command) {
        mars.getRobot().execute(command);
    }

    public String whereisRobot() {
        return mars.getRobot().toJSON();
    }
}
