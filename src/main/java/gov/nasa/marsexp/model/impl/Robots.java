package gov.nasa.marsexp.model.impl;

import gov.nasa.marsexp.model.IRobot;
import gov.nasa.marsexp.model.ISurface;
import gov.nasa.marsexp.model.geo.Coordinate;
import gov.nasa.marsexp.model.geo.Position;

/**
 * Robot objects are singleton.
 *
 * This class is an implementations of the static factory method (not GoF pattern), and has the purpose
 * of isolate robots implementation from the clients and cache robots instance. Can be evolved for more
 * elaborated robots management (like handling n robots).
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public final class Robots {

    public static IRobot opportunity; // cache
    private Robots() {} // prevents instantiation

    /**
     * Returns an instance of gov.nasa.marsexp.model.impl.Robot. If there's already an instance of robot cached,
     * will return the cached instance. Otherwise, will create a new robot instance.
     *
     * @param surface The surface where robot will be in.
     * @param coordinate The start coordinate of the robot.
     * @param position The start position of the robot.
     * @return Cached instance of robot, or
     */
    public static IRobot newInstance(ISurface surface, Coordinate coordinate, Position position) {
        if (opportunity == null) {
            opportunity = new Robot(surface, coordinate, position);
        }
        return opportunity;
    }
}
