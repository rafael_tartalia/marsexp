package gov.nasa.marsexp.model.geo;

/**
 * This class represents a cardinal direction, and can only represent directions regarding to north, east,
 * west, and south. Other directions like northeast or southeast are not allowed. So, direction degree
 * can change by 90 degrees only.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public final class Position {

    private Direction direction;

    /**
     * Constructs a Position object in the specific Direction.
     *
     * @param direction The direction of the position.
     */
    public Position(Direction direction) {
        if (direction == null) {
            throw new IllegalArgumentException("Direction could not be null");
        }
        this.direction = direction;
    }

    /**
     * Change the current Direction.
     *
     * @param degree A degree - 0, 90, 180, or 360.
     */
    public void setDirectionTo(int degree) {
        Direction direction = null;

        for (Direction e : Direction.values()) {
            if (e.getDegree() == degree) {
                direction = e;
            }
        }

        if (direction == null) {
            throw new IllegalArgumentException("Position should be 0, 90, 180, or 360");
        }

        this.direction = direction;
    }

    /**
     * Returns the direction of the position.
     *
     * @return The direction.
     */
    public Direction getDirection() {
        return direction;
    }

    /**
     * Return the degree of the direction. It's a wrapper method only.
     *
     * @return The degree of the direction.
     */
    public int getDegree() {
        return direction.getDegree();
    }

    @Override
    public String toString() {
        return direction.toString();
    }

    /**
     * A public Enum that represents valid Directions.
     */
    public enum Direction {

        NORTH('N', 0), EAST('E', 90), SOUTH('S', 180), WEAST('W', 270);

        private char identity;
        private int degree;

        Direction(char identity, int position) {
            this.identity = identity;
            this.degree = position;
        }

        public char getIdentity() {
            return identity;
        }

        public int getDegree() {
            return degree;
        }

        @Override
        public String toString() {
            return "Direction {" +
                    "identity=" + identity +
                    ", degree=" + degree +
                    '}';
        }
    }
}
