marsexp:
========================

Design and Architecture Assumptions and Principles
---------------------------------------------------
1. marsexp works only for one robot, although it could be evolved for n robots in a easy way
2. The services returns JSON (I was in doubt if it should return PLAIN TEXT, but choose REST) 
3. When a failure occur, the services returns clear messages for troubleshoot purpose; The HTTP Header contain the main cause (e.g. 403 - BAD REQUEST)
4. Robot and Mars are singletons, mutable objects, and are acessible through static factories
5. Mutability was reduced at the maximum in objetcs
6. The methods related to Robot and Mars that can change state, are synchronized, to preserve consistent state (thread safety)
7. Favor the use of the standard exceptions
8. Minimize scopo of local variables
9. Refer to objects by their interfaces
10. Check parameter for validity


System requirements
-------------------

This project was built with Java 7.0 (Java SDK 1.7), and Maven 3.1.


Start JBoss WildFly with the Web Profile
-------------------------

1. Open a command line and navigate to the root of the JBoss server directory.
2. The following shows the command line to start the server with the web profile:

        For Linux:   JBOSS_HOME/bin/standalone.sh
        For Windows: JBOSS_HOME\bin\standalone.bat

 
Build and Deploy the Quickstart
-------------------------

_NOTE: The following build command assumes you have configured your Maven user settings. If you have not, you must include Maven setting arguments on the command line. See [Build and Deploy the Quickstarts](https://github.com/jboss-developer/jboss-eap-quickstarts#build-and-deploy-the-quickstarts) for complete instructions and additional options._

1. Make sure you have started the JBoss Server as described above.
2. Open a command line and navigate to the root directory of this quickstart.
3. Type this command to build and deploy the archive:

        mvn clean package wildfly:deploy

4. This will test, package and deploy `target/marsexp.war` to the running instance of the server.

Note: This project is tested only with JUnit.
 
Access the application
---------------------

The application will be running at the following URL: <http://localhost:8080/marsexp/>.

The valid URLs that can be accessed are:

http://localhost:8080/marsexp/rest/mars/whereis/robot
http://localhost:8080/marsexp/rest/mars/{command}

Where the first one returns the current position of the robot, and the second send commands to the robot.

The robot accept commands to turn-left, turn-right, and to move (L, R, M).

Test the application
---------------------

Send a command to the robot:

```
#!bash

$ curl -v --request POST http://localhost:8080/marsexp/rest/mars/MMRMMRMM; echo

* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /marsexp/rest/mars/MMRMMRMM HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 200 OK
< Connection: keep-alive
< X-Powered-By: Undertow/1
* Server WildFly/9 is not blacklisted
< Server: WildFly/9
< Content-Type: application/json
< Content-Length: 28
< Date: Mon, 02 Nov 2015 04:48:38 GMT
< 
* Connection #0 to host localhost left intact
{"x": "2", "y": "0", "direction": "S"}
```

Get current robot position:

```
#!bash

$ curl -v --request GET http://localhost:8080/marsexp/rest/mars/whereis/robot; echo
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /marsexp/rest/mars/whereis/robot HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 200 OK
< Connection: keep-alive
< X-Powered-By: Undertow/1
* Server WildFly/9 is not blacklisted
< Server: WildFly/9
< Content-Type: application/json
< Content-Length: 38
< Date: Mon, 02 Nov 2015 04:52:26 GMT
< 
* Connection #0 to host localhost left intact
{"x": "2", "y": "0", "direction": "S"}`
```

Executing an invalid command:

```
#!bash

$ curl -v --request POST http://localhost:8080/marsexp/rest/mars/AAAA; echo
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /marsexp/rest/mars/AAAA HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 400 Bad Request
< Connection: keep-alive
< X-Powered-By: Undertow/1
* Server WildFly/9 is not blacklisted
< Server: WildFly/9
< Content-Type: application/json
< Content-Length: 32
< Date: Mon, 02 Nov 2015 04:53:13 GMT
< 
* Connection #0 to host localhost left intact
{"error":"Invalid command AAAA"}
```

Executing an invalid command (invalid position in mars):

```
#!bash

$ curl -v --request POST http://localhost:8080/marsexp/rest/mars/MMMMMMM; echo
* Hostname was NOT found in DNS cache
*   Trying 127.0.0.1...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> POST /marsexp/rest/mars/MMMMMMM HTTP/1.1
> User-Agent: curl/7.35.0
> Host: localhost:8080
> Accept: */*
> 
< HTTP/1.1 400 Bad Request
< Connection: keep-alive
< X-Powered-By: Undertow/1
* Server WildFly/9 is not blacklisted
< Server: WildFly/9
< Content-Type: application/json
< Content-Length: 69
< Date: Mon, 02 Nov 2015 04:54:04 GMT
< 
* Connection #0 to host localhost left intact
{"error":"Invalid move (probably you are moving to an invalid area)"}
```

Undeploy the Archive
--------------------

1. Make sure you have started the JBoss Server as described above.
2. Open a command line and navigate to the root directory of this quickstart.
3. When you are finished testing, type this command to undeploy the archive:

        mvn wildfly:undeploy
