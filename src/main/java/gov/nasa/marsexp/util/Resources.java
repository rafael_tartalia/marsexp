package gov.nasa.marsexp.util;

import gov.nasa.marsexp.model.IRobot;
import gov.nasa.marsexp.model.ISurface;
import gov.nasa.marsexp.model.geo.Coordinate;
import gov.nasa.marsexp.model.geo.Position;
import gov.nasa.marsexp.model.impl.Robots;
import gov.nasa.marsexp.model.impl.Surfaces;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import java.util.logging.Logger;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence context, to CDI beans
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public class Resources {

    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }

    @Produces
    public ISurface produceMars() {
        Coordinate coordinate = new Coordinate(0, 0);
        Position position = new Position(Position.Direction.NORTH);
        ISurface mars = Surfaces.newMarsInstance(5, 5);
        IRobot opportunity = Robots.newInstance(mars, coordinate, position);
        mars.addRobot(opportunity);
        return mars;
    }
}
