package gov.nasa.marsexp.model.impl;

import gov.nasa.marsexp.model.ISurface;

/**
 * Mars objects are singleton.
 *
 * This class is an implementations of the static factory method (not GoF pattern), and has the purpose
 * of isolate surface implementation from the clients and cache surface instance. Can be evolved for more
 * elaborated surface management.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 *
 */
public final class Surfaces {

    private static ISurface mars; // mars cache
    private Surfaces() {} //prevents instantiation

    /**
     * Creates and cache a instance of Mars.
     *
     * @param n The total lines of Mars.
     * @param m The total columns of Mars.
     * @return New instance of Mars, or the cached instance.
     */
    public static ISurface newMarsInstance(int n, int m) {
        if (mars == null) {
            mars = new Mars(n, m);
        }
        return mars;
    }
}
