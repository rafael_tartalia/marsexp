package gov.nasa.marsexp.model;

import gov.nasa.marsexp.model.geo.Coordinate;

/**
 * This interface represents a surface (like Mars).
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public interface ISurface {

    /**
     * This method evaluates an arbitrary coordinate to verify if it is valid.
     * Regarding to the context, implementer classes can evaluate if there's an obstacle in the coordinate
     * or if the coordinate is valid regarding the surface.
     *
     * @param coordinate An arbitrary coordinate.
     * @return boolean is the coordinate is valid, false otherwise.
     */
    boolean evaluate(Coordinate coordinate) ;


    /**
     * Add a robot to the surface.
     *
     * @param robot The robot.
     */
    void addRobot(IRobot robot);

    /**
     * Returns a robot by name.
     *
     * @return The robot in the surface.
     */
    IRobot getRobot();
}
