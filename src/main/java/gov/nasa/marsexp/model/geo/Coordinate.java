package gov.nasa.marsexp.model.geo;

/**
 * This class represents a coordinate (positive or negative)
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public final class Coordinate {

    private int x = 0;
    private int y = 0;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // just getters and setters
    public void setX(int x) { this.x = x; }
    public void setY(int y) { this.y = y; }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
