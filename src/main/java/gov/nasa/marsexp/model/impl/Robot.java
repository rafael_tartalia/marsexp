package gov.nasa.marsexp.model.impl;

import gov.nasa.marsexp.model.IRobot;
import gov.nasa.marsexp.model.ISurface;
import gov.nasa.marsexp.model.geo.Coordinate;
import gov.nasa.marsexp.model.geo.Position;

/**
 * This abstraction represents a robot. A robot have a position, a coordinate, can move, turn left, and turn right.
 * The robot also know about the surface where he's located, and can move in a smart way. The robot moves only up
 * to front, regarding the current position.
 *
 * Robots aren't accessible to the outside world.
 *
 * @see gov.nasa.marsexp.model.impl.Robots to get an Robot instance.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
final class Robot implements IRobot {
    private static Position position;
    private static Coordinate coordinate;
    private static ISurface surface;

    public enum Commands {L, R, M}

    public Robot(ISurface surface, Coordinate coordinate, Position position) {
        if (position == null || coordinate == null || surface == null) {
            throw new IllegalArgumentException("Position, coordinate and mars could not be null");
        }

        this.position = position;
        this.coordinate = coordinate;
        this.surface = surface;
    }

    @Override
    public synchronized void execute(String commands) {
        if (commands == null || commands.isEmpty()) {
            throw new IllegalArgumentException("Invalid command (null or empty)");
        }

        char[] commandBuff = new char[commands.length()];
        for (int i = 0; i < commands.length(); i++) {
            commandBuff[i] = commands.charAt(i);
        }

        // if all commands are valid
        if (!allCommandsAreValid(commandBuff)) {
            throw new IllegalArgumentException("Invalid command " + commands);
        }

        // and evaluate commands - it's like a pre-plan
        if (!evaluateMove(commandBuff)) {
            throw new IllegalArgumentException("Invalid move (probably you are moving to an invalid area)");
        }

        // everything is ok at this point, so let's move
        executeCommands(commandBuff);
    }

    @Override
    public synchronized Position getPosition() {
        return position;
    }

    @Override
    public synchronized Coordinate getCoordinate() {
        return coordinate;
    }

    @Override
    public String toJSON() {
        StringBuilder sb = new StringBuilder()
                .append("{")
                .append("\"x\": \"")
                .append(coordinate.getX())
                .append("\", \"y\": \"")
                .append(coordinate.getY())
                .append("\", \"direction\": \"")
                .append(position.getDirection().getIdentity())
                .append("\"}");
        return sb.toString();
    }

    @Override
    public String toString() {
        return "Robot{" +
                "position=" + position +
                ", coordinate=" + coordinate +
                ", surface=" + surface +
                '}';
    }

    /**
     * This method do a 'execution plan', verifying if the move is valid. In some way, this guarantee that
     * the robot will only move if the command is valid (move the robot to a valid position).
     *
     * @return
     */
    private boolean evaluateMove(char[] commandsBuff) {
        // avoid improper mutation - clone position and coordinate to check reliability
        Position position = new Position(this.position.getDirection());
        Coordinate coordinate = new Coordinate(this.coordinate.getX(), this.coordinate.getY());

        for (char command : commandsBuff) {
            coordinate = executeCommand(position, coordinate, Commands.valueOf(String.valueOf(command)));
        }

        // test coordinate against the surface
        return surface.evaluate(coordinate);
    }

    /**
     * This method checks if all commands are valid (L,R or M).
     *
     * @return True if all commands are valid, false is there's one invalid command.
     */
    private boolean allCommandsAreValid(char[] commands) {
        for (char command : commands) {
            try {
                Commands.valueOf(Character.toString(command));
            } catch (IllegalArgumentException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method execute the commands effectively, with current robot position and coordinate.
     */
    private void executeCommands(char[] commands) {
        for (char command : commands) {
            executeCommand(position, coordinate, Commands.valueOf(Character.toString(command)));
        }
    }

    /**
     * This method execute a simple command and return a coordinate.
     */
    private Coordinate executeCommand(Position position, Coordinate coordinate, Commands command) {
        switch (command) {
            case M:
                move(coordinate, position);
                break;
            case L:
                turnLeft(position);
                break;
            case R:
                turnRight(position);
                break;
        }
        return coordinate;
    }

    private void move(Coordinate coordinate, Position position) {
        switch (position.getDirection()) {
            case NORTH:
                coordinate.setY(coordinate.getY() + 1);
                break;
            case SOUTH:
                coordinate.setY(coordinate.getY() - 1);
                break;
            case EAST:
                coordinate.setX(coordinate.getX() + 1);
                break;
            case WEAST:
                coordinate.setX(coordinate.getX() - 1);
        }
    }

    private void turnLeft(Position position) {
        int positionDegree = position.getDegree();
        if (positionDegree == 0) { positionDegree = 360; }
        positionDegree = positionDegree - 90;
        position.setDirectionTo(positionDegree);
    }

    private void turnRight(Position position) {
        int positionDegree = position.getDegree();
        positionDegree = (positionDegree + 90) % 360;
        position.setDirectionTo(positionDegree);
    }
}
