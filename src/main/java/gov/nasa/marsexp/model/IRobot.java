package gov.nasa.marsexp.model;

import gov.nasa.marsexp.model.geo.Coordinate;
import gov.nasa.marsexp.model.geo.Position;

/**
 * This interface represents a robot. A robot can receive commands, and let other objects know where he is.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
public interface IRobot {

    /**
     * Execute an arbitrary command.
     *
     * @param commands A command to execute.
     */
     void execute(String commands);

    /**
     * Returns the current position.
     * @see gov.nasa.marsexp.model.geo.Position
     *
     * @return The current position.
     */
    Position getPosition();

    /**
     * Returns the current coordinate.
     * @see gov.nasa.marsexp.model.geo.Coordinate
     *
     * @return The current coordinate.
     */
    Coordinate getCoordinate();

    /**
     * Returns a representations of the robot in JSON.
     *
     * @return A JSON object
     */
    String toJSON();
}
