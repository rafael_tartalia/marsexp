package gov.nasa.marsexp.rest;

import gov.nasa.marsexp.service.MarsExplorerService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * A robot resource accessible through the rest interface.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
@Path("/")
@RequestScoped
public class MarsResource {

    @Inject
    private Logger log;

    @Inject
    private MarsExplorerService marsExplorerService;

    @POST
    @Path("/mars/{command}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response executeCommand(@PathParam("command") String command) {
        Response.ResponseBuilder builder;
        try {
            log.info("Received command: " + command);
            marsExplorerService.execute(command);
            builder = Response.status(Response.Status.OK).entity(marsExplorerService.whereisRobot());
        } catch (IllegalArgumentException e) {
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        } catch (RuntimeException e) {
            Map<String, String> responseObj = new HashMap<>();
            String message = "Unfortunately we had a problem (" + e.getMessage() + ")";
            responseObj.put("error", message);
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseObj);
        }
        return builder.build();
    }

    @GET
    @Path("/mars/whereis/robot")
    @Produces(MediaType.APPLICATION_JSON)
    public Response whereisRobot() {
        Response.ResponseBuilder builder;
        try {
            log.info("Asked where is the robot");
            builder = Response.status(Response.Status.OK).entity(marsExplorerService.whereisRobot());
        } catch (RuntimeException e) {
            Map<String, String> responseObj = new HashMap<>();
            String message = "Unfortunately we had a problem (" + e.getMessage() + ")";
            responseObj.put("error", message);
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(responseObj);
        }
        return builder.build();
    }
}
