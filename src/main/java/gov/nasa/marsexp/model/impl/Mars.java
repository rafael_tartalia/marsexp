package gov.nasa.marsexp.model.impl;

import gov.nasa.marsexp.model.IRobot;
import gov.nasa.marsexp.model.ISurface;
import gov.nasa.marsexp.model.geo.Coordinate;

/**
 * This abstraction represents Mars. There's one robot in Mars.
 * For efficiency purposes, there's no representations of the entire surface of Mars, so space complexity of this
 * abstration is constant, or O(1), regarding N and M.
 *
 * @author Rafael Tartalia (rafael.tartalia@gmail.com)
 */
final class Mars implements ISurface {
    private static IRobot robot;
    private static int yBoundary;
    private static int xBoundary;

    /**
     * Constructs mars surface with n rows and m columns.
     *
     * @param n The number of lines of mars.
     * @param m The number of columns of mars.
     */
    public Mars(int n, int m) {
        if (n <= 0 || m <= 0) {
            throw new IllegalArgumentException("x and y should be greater than 0");
        }
        this.yBoundary = n; //lines
        this.xBoundary = m; //columns
    }

    @Override
    public boolean evaluate(Coordinate coordinate) {
        if (coordinate == null) {
            new IllegalArgumentException("coordinate could not be null");
        }

        if (coordinate.getX() >= xBoundary || coordinate.getY() >= yBoundary
                || coordinate.getX() < 0 || coordinate.getY() < 0) {
            return false;
        }
        return true;
    }

    @Override
    public void addRobot(IRobot robot) {
        this.robot = robot;
    }

    @Override
    public IRobot getRobot() {
        return robot;
    }
}
